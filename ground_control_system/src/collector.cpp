/*
  collector
  Launch a ROS node to subscribe topics.
  @author  Yolanda de la Hoz Simón
  @date    03-2015
  @version 1.0
*/

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <QDebug>
#include <ros/network.h>
#include <string>
#include <std_msgs/String.h>
#include <sstream>
#include "../include/ground_control_system/collector.hpp"


/*****************************************************************************
** Implementation
*****************************************************************************/

Collector::Collector(int argc, char** argv ) :
	init_argc(argc),
	init_argv(argv)
    {}

Collector::~Collector() {
    if(ros::isStarted()) {
      ros::shutdown(); // Kill all open subscriptions, publications, service calls, and service servers.
      ros::waitForShutdown();
    }
	wait();
}

bool Collector::init() {
    ros::init(init_argc,init_argv,"Listener");// ros node started.
    if ( ! ros::master::check() ) {// Check if roscore has been initialized.
		return false;
	}

    ros::start(); // explicitly call to ros start
    ros::NodeHandle n;
    // Topic communications
    // Subscribe a Topic called "chatter"
    SimulatedStateSubs = n.subscribe("/Simulated_state", 1, &Collector::simulatedStateCallback,this);
    PoseChangesSubs = n.subscribe("/Pose_changes", 1, &Collector::poseChangesCallback,this);
    PerceivedStateError1Subs = n.subscribe("/Perceived_state_error1", 1, &Collector::perceivedStateError1Callback,this);
    PerceivedStateError2Subs = n.subscribe("/Perceived_state_error2", 1, &Collector::perceivedStateError2Callback,this);
    PerceivedStateError3Subs = n.subscribe("/Perceived_state_error3", 1, &Collector::perceivedStateError3Callback,this);
    PerceivedStateError4Subs = n.subscribe("/Perceived_state_error4", 1, &Collector::perceivedStateError4Callback,this);

    start();
//    real_time=ros;
    return true;
}


void Collector::run() {
    ros::spin();
    std::cout << "Ros shutdown, proceeding to close the gui." << std::endl;
    Q_EMIT rosShutdown(); // used to signal the gui for a shutdown (useful to roslaunch)
}

// Connect with a given master url and host url
bool Collector::init(const std::string &master_url, const std::string &host_url) {
	std::map<std::string,std::string> remappings;
	remappings["__master"] = master_url;
	remappings["__hostname"] = host_url;
    ros::init(remappings,"Listener");
	if ( ! ros::master::check() ) {
		return false;
	}
    ros::start(); // explicitly call to ros start
	ros::NodeHandle n;
    // Topic communications
    // Subscribe a Topic called "chatter"
   // gcs_subscriber = n.subscribe("chatter", 1000, &Collector::chatterCallback,this);
	return true;
}


void Collector::simulatedStateCallback(const common::SimulatedState::ConstPtr& msg) {
   posx=  msg->uav.position.x;
   posy=  msg->uav.position.y;
   posz=  msg->uav.position.z;
   //ROS_INFO("Received posx from Simulated State topic: [%f]", posx);
   //ROS_INFO("Received posy from Simulated State topic: [%f]", posy);
   //ROS_INFO("Received posz from Simulated State topic: [%f]", posz);
   Q_EMIT parameterReceived();

}

void Collector::poseChangesCallback(const common::PoseChanges::ConstPtr& msg) {
   accelx=  msg->dx;
   accely=  msg->dy;
   accelz=  msg->dy;
   //ROS_INFO("Received accelx from Pose Changes topic: [%f]", accelx);
   //ROS_INFO("Received accely from Pose Changes topic: [%f]", accely);
   //ROS_INFO("Received accelz from Pose Changes topic: [%f]", accelz);
}

void Collector::perceivedStateError1Callback(const common::PerceivedState::ConstPtr& msg) {
    e1posx=  msg->uav.position.x;
    e1posy=  msg->uav.position.y;
    e1posz=  msg->uav.position.z;
    //ROS_INFO("Received posx from Perceived State topic: [%f]", e1posx);
    //ROS_INFO("Received posy from Perceived State topic: [%f]", e1posy);
    //ROS_INFO("Received posz from Perceived State topic: [%f]", e1posz);
}void Collector::perceivedStateError2Callback(const common::PerceivedState::ConstPtr& msg) {
    e2posx=  msg->uav.position.x;
    e2posy=  msg->uav.position.y;
    e2posz=  msg->uav.position.z;
    //ROS_INFO("Received posx from Perceived State topic: [%f]", e2posx);
    //ROS_INFO("Received posy from Perceived State topic: [%f]", e2posy);
    //ROS_INFO("Received posz from Perceived State topic: [%f]", e2posz);
}void Collector::perceivedStateError3Callback(const common::PerceivedState::ConstPtr& msg) {
    e3posx=  msg->uav.position.x;
    e3posy=  msg->uav.position.y;
    e3posz=  msg->uav.position.z;
   // ROS_INFO("Received posx from Perceived State topic: [%f]", e3posx);
   // ROS_INFO("Received posy from Perceived State topic: [%f]", e3posy);
   // ROS_INFO("Received posz from Perceived State topic: [%f]", e3posz);
}void Collector::perceivedStateError4Callback(const common::PerceivedState::ConstPtr& msg) {
    e4posx=  msg->uav.position.x;
    e4posy=  msg->uav.position.y;
    e4posz=  msg->uav.position.z;
  //  ROS_INFO("Received posx from Perceived State topic: [%f]", e4posx);
  //  ROS_INFO("Received posy from Perceived State topic: [%f]", e4posy);
  //  ROS_INFO("Received posz from Perceived State topic: [%f]", e4posz);
}
void Collector::log( const LogLevel &level, const std::string &msg) {
	logging_model.insertRows(logging_model.rowCount(),1);
	std::stringstream logging_model_msg;
	switch ( level ) {
		case(Debug) : {
				ROS_DEBUG_STREAM(msg);
				logging_model_msg << "[DEBUG] [" << ros::Time::now() << "]: " << msg;
				break;
		}
		case(Info) : {
				ROS_INFO_STREAM(msg);
				logging_model_msg << "[INFO] [" << ros::Time::now() << "]: " << msg;
				break;
		}
		case(Warn) : {
				ROS_WARN_STREAM(msg);
				logging_model_msg << "[INFO] [" << ros::Time::now() << "]: " << msg;
				break;
		}
		case(Error) : {
				ROS_ERROR_STREAM(msg);
				logging_model_msg << "[ERROR] [" << ros::Time::now() << "]: " << msg;
				break;
		}
		case(Fatal) : {
				ROS_FATAL_STREAM(msg);
				logging_model_msg << "[FATAL] [" << ros::Time::now() << "]: " << msg;
				break;
		}
	}
	QVariant new_row(QString(logging_model_msg.str().c_str()));
	logging_model.setData(logging_model.index(logging_model.rowCount()-1),new_row);
	Q_EMIT loggingUpdated(); // used to readjust the scrollbar
}


