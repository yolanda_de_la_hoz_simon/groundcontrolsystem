#include "../include/ground_control_system/processmonitor.hpp"
#include "../../ground_control_system-build/ui_processmonitor.h"

processMonitor::processMonitor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::processMonitor)
{
    ui->setupUi(this);

    // ESTRUCTURA DE VISUALIZACIÓN
    QStringList mission_planner;
    mission_planner << "Mission Planner";
    QStringList flight_controller;
    flight_controller << "Flight Controller";
    QStringList drivers;
    drivers << "odometry_driver" << "camera_driver" << "pilot driver";
    QStringList computer_vision;
    computer_vision << "camera_processor"<<"robot_detector"<<"grid_detector";
    QStringList state_estimator;
    state_estimator << "state_map_estimator" << "pose_estimator";

    processList.insert("Mission Planner",mission_planner);//pos1
    processList.insert("Flight Controller",flight_controller);//pos3
    processList.insert("Drivers",drivers);//pos2
    processList.insert("Computer Vision",computer_vision);//pos2
    processList.insert("State Estimator",state_estimator);//pos2

    this->initTree(processList,ui->treeWidget);


}

void processMonitor::setSignalHandlers()
{

}
void processMonitor::initTree(QMap<QString,QStringList> algorithmsList, QTreeWidget *tree){
    QMapIterator<QString,QStringList> i(algorithmsList);
    while (i.hasNext()) {
        i.next();
        //qDebug() << i.key();
        this->addRootTree(i.key(),i.value(), tree);
    }
}

void processMonitor::addRootTree(QString name, QStringList list, QTreeWidget *tree){
    QTreeWidgetItem  *itm = new QTreeWidgetItem(tree);
    itm->setText(0,name);

    itm->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled );
    itm->setCheckState(1,Qt::Unchecked);
    itm->setCheckState(2,Qt::Unchecked);
    tree->addTopLevelItem(itm);
    addChildTree(itm,list,"");

}
void processMonitor::addChildTree(QTreeWidgetItem *parent,  QStringList list, QString description){
    for(int i =0;i<list.size();++i){
        QTreeWidgetItem *itm = new QTreeWidgetItem();
        itm->setText(0,list.at(i));

        parent->addChild(itm);
    }
}
processMonitor::~processMonitor()
{
    delete ui;
}
