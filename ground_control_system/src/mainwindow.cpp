/*
  MainWindow
  Main window template and pop-up windows signals handlers.
  @author  Yolanda de la Hoz Simón
  @date    03-2015
  @version 1.0
*/

#include "../include/ground_control_system/mainwindow.hpp"
#include "../../ground_control_system-build/ui_mainwindow.h"
#include <qwt/qwt.h>
#include <qwt/qwt_plot.h>
#include <qwt/qwt_plot_curve.h>
#include <qwt/qwt_text.h>
#include <QWidget>

MainWindow::MainWindow(int argc, char** argv,QWidget *parent) :
    QMainWindow(parent),
    collector(argc,argv),
    usercommander(argc,argv),
    ui(new Ui::MainWindow) //initialize ui member
{
    ui->setupUi(this);// connects all ui's triggers

    //Initialize views.

    processView = new processMonitor(this);
    ui->gridLayout_10->addWidget(processView,0,0);
    paramPlot = new parameterTemporalSeries(this,&collector);
    ui->gridLayout_5->addWidget(paramPlot,0,0);

    connection= new Connection(this,&collector,&usercommander);

    percept = new PerceptionView(this);
    ui->gridLayout_7->addWidget(percept,0,0);

    sphere = new sphereView(this);
    ui->gridLayout_14->addWidget(sphere,0,0);

    dinamic= new dinamicsView(this);
    ui->gridLayout_2->addWidget( dinamic,0,0);



    //Initialize threads
    usercommander.init();
    collector.init();

    //QObject::connect(this, SIGNAL(lastWindowClosed()), this, SLOT(close()));
    connect(&collector, SIGNAL(rosShutdown()), this, SLOT(close())); //Close the app.

    connect(ui->pushButton_start,SIGNAL(clicked()),this, SLOT(onStartButton()));
    connect(ui->pushButton_hover,SIGNAL(clicked()),this, SLOT(onHoverButton()));
    connect(ui->pushButton_EmergencyStop,SIGNAL(clicked()),this, SLOT(onEmergencyStopButton()));
    connect(ui->pushButton_land,SIGNAL(clicked()),this, SLOT(onLandButton()));
    connect(ui->pushButton_yawZero,SIGNAL(clicked()),this, SLOT(onYawZeroButton()));
    connect(ui->comboBox_3,SIGNAL(currentIndexChanged(QString)),this, SLOT(onControlModeChange(QString)));


    setWindowIcon(QIcon(":/images/images/drone-icon.png"));
    this->setWindowTitle("Ground Control System");
}

MainWindow::~MainWindow()
{
    delete ui;

}
void MainWindow::close()
{
    this->~MainWindow();
}


void MainWindow::on_actionAbout_Ground_Control_System_triggered() {
    QMessageBox::about(this, tr("About ..."),tr("<h2>Ground Control System</h2><p>Universidad Politecnica de Madrid</p><p>blablabla aplication description</p>"));
}

void MainWindow::on_actionContents_triggered() {
    QMessageBox::about(this, tr("Contents ..."),tr("<h2>Ground Control System</h2><p>Universidad Politecnica de Madrid</p><p>blablabla aplication description</p>"));
}

void MainWindow::on_actionUser_Commands_Manual_triggered() {
    QMessageBox::about(this, tr("Manual Control Guided ..."),tr("<h2>Ground Control System</h2><p>Keyboard Bindings List</p><p>TAKE_OFF = t</p><p>LAND = y</p><p>EMERGENCY_STOP = space</p><p>HOVER = h</p><p>MOVE = m</p><p>RESET_COMMANDS= s</p><p>MOVE_UPWARDS = q</p><p>MOVE_DOWNWARDS = a</p><p>TURN_COUNTER_CLOCKWISE = z</p><p>TURN_CLOCKWISE = x</p><p>SET_YAW_REFERENCE_TO_0 = backslash</p><p>MOVE_FORWARD = up </p><p>MOVE_BACK = down</p><p>MOVE_RIGHT = right</p><p>MOVE_LEFT = left</p>"));
}


void MainWindow::on_actionNew_connection_triggered()
{
    connection = new Connection();
    connection->setWindowTitle("New Connection");
    connection->show();
}


void MainWindow::on_action3D_Perception_View_triggered()
{
    percept = new PerceptionView(this);
    percept->setWindowTitle("Perception View");
    percept->show();

}

void MainWindow::on_actionParameter_Temporal_Series_triggered()
{
    paramPlot = new parameterTemporalSeries(this,&collector);
    paramPlot->setWindowTitle("Parameter Temporal Series");
    //paramPlot->parameterTemporalSeries-
    paramPlot->show();

}
// Control Mode
void MainWindow::onControlModeChange(QString mode){
   // usercommander.msg.type = mode.str();
}

// User commands
void MainWindow::onStartButton(){
    std::stringstream key;
    key << "t";
    usercommander.msg.type = key.str();

}
void MainWindow::onHoverButton(){
    std::stringstream key;
    key << "h";
     usercommander.msg.type  = key.str();

}
void MainWindow::onEmergencyStopButton(){
    std::stringstream key;
    key << "space";
     usercommander.msg.type  = key.str();

}
void MainWindow::onLandButton(){
    std::stringstream key;
    key << "y";
     usercommander.msg.type  = key.str();

}
void MainWindow::onYawZeroButton(){
    std::stringstream key;
    key << "backlash";
     usercommander.msg.type  = key.str();

}
