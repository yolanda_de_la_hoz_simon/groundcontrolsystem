/*
  2D Parameter temporal series window
  Plotting of odometer parameters selected in real time.
  @author  Yolanda de la Hoz Simón
  @date    03-2015
  @version 1.0
*/

#include "../include/ground_control_system/parametertemporalseries.hpp"
#include "../../ground_control_system-build/ui_parametertemporalseries.h"
#include "../include/ground_control_system/parameter.hpp"

#include <qwt/qwt.h>
#include <qwt/qwt_plot.h>
#include <qwt/qwt_plot_curve.h>
#include <qwt/qwt_text.h>
#include <qwt/qwt_plot_magnifier.h>
#include <qwt/qwt_counter.h>
#include <cmath>

#include <QLabel>
#include <QLayout>
#include <QDockWidget>
#include <QWidget>
#include <QCheckBox>
#include <QVariant>
#include <list>
#include <QMap>
#include <QMapIterator>
#include <QRegExp>


parameterTemporalSeries::parameterTemporalSeries(QWidget *parent, Collector* collector):
    QDockWidget(parent),
    ui(new Ui::parameterTemporalSeries)
{
    ui->setupUi(this);
    Collector* node=collector;
    plot = new DataPlot(ui->widget,node);

    ui->widget->resize(1079,572);
    printf ("height: %d \n",ui->widget->size().height());
    printf ("width: %d \n",ui->widget->size().width());
    plot->setCanvasBackground(QBrush(Qt::white));

    // ESTRUCTURA DE VISUALIZACIÓN
    QStringList telemetry;
    telemetry << "pos.X" << "pos.Y" << "pos.Z" << "yaw IMU" << "pitch IMU" << "yawspeed IMU" << "roll IMU"<< "Accel.X" << "Accel.Y" << "Accel.Z"  << "GroundSpeed"<< "Mag.X"<< "Mag.Y"<< "Mag.Z" << "Gyro Theta" << "Gyro Psi" << "Gyro Phi" << "Temperature" << "Press.Abs" << "Press.Diff"<< "Battery" ;
    QStringList ekf;
    ekf << "pos.X" << "pos.Y" << "pos.Z";
    QStringList controller;
    controller << "xci" << "yci" << "zci" << "yawci" << "vxfi" << "vyfi"<< "vzfi" << "dyawfi";
    QStringList arucoSlam;
    arucoSlam << "pos.X" << "pos.Y" << "pos.Z";

    parameters.insert("Telemetry",telemetry);//pos1
    parameters.insert("Aruco Slam",arucoSlam);//pos3
    parameters.insert("Ext.Kallman Filter",ekf);//pos4
    parameters.insert("Controller",controller);//pos2

    this->initTree(parameters,ui->treeWidget);
    //this->initParameterList(paramsTelemetry,ui->treeWidget);

    // this->initTree(sensorData,ui->treeWidget);


    connect(ui->treeWidget,SIGNAL(itemChanged(QTreeWidgetItem*,int)),
            plot,SLOT(clickToPlot(QTreeWidgetItem*, int)));
    connect(ui->spinBox_2, SIGNAL(valueChanged(int)),
            plot, SLOT(resizeAxisYScale(int)) );
    connect(ui->pushButton, SIGNAL(clicked()),
            plot, SLOT(saveAsSVG()) );
  //  connect(ui->lineEdit,SIGNAL(textChanged(QString)),
   //         this, SLOT(onTextFilterChange(QString)));
    connect(ui->checkBox_4,SIGNAL(clicked(bool)),
            this, SLOT(onShowUnits(bool)));
    connect(ui->pushButton_2,SIGNAL(clicked()),
            this, SLOT(onStopButton()));
}

void parameterTemporalSeries::setSignalHandlers()
{

}

void parameterTemporalSeries::resizeEvent(QResizeEvent * event)
{
    plot->resize(ui->widget->size());
}
void parameterTemporalSeries::initParameterList(QStringList list, QTreeWidget *tree){

    ui->treeWidget->setIndentation(15);
    for(int i =0;i<list.size();++i){
        QTreeWidgetItem  *itm = new QTreeWidgetItem(ui->treeWidget);
        QPixmap pixmap(15,40);
        pixmap.fill(QColor("white"));
        QIcon redIcon(pixmap);
        itm->setIcon(0, redIcon);
        itm->setText(0,list.at(i));
        itm->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled );
        itm->setCheckState(0,Qt::Unchecked);
        ui->treeWidget->addTopLevelItem(itm);
    }
}


void parameterTemporalSeries::initTree(QMap<QString,QStringList> algorithmsList, QTreeWidget *tree){
    QMapIterator<QString,QStringList> i(algorithmsList);
    while (i.hasNext()) {
        i.next();
        qDebug() << i.key();
        this->addRootTree(i.key(),i.value(), tree);
    }
}
void parameterTemporalSeries::addRootTree(QString name, QStringList list, QTreeWidget *tree){
    QTreeWidgetItem  *itm = new QTreeWidgetItem(tree);
    itm->setText(0,name);
    tree->addTopLevelItem(itm);
    tree->setIndentation(15);
    addChildTree(itm,list,"");

}
void parameterTemporalSeries::addChildTree(QTreeWidgetItem *parent,  QStringList list, QString description){
    for(int i =0;i<list.size();++i){
        QTreeWidgetItem *itm = new QTreeWidgetItem();
        QPixmap pixmap(15,40);
        pixmap.fill(QColor("white"));
        QIcon redIcon(pixmap);
        itm->setIcon(0, redIcon);
        itm->setText(0,list.at(i));
        itm->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled );
        itm->setCheckState(0,Qt::Unchecked);

        parent->addChild(itm);
    }
}

void parameterTemporalSeries::onTextFilterChange(const QString &arg1){
    QRegExp regExp(arg1, Qt::CaseInsensitive, QRegExp::Wildcard);
    ui->treeWidget->blockSignals(true);
    ui->treeWidget->clear();

    QMapIterator<QString,QStringList> i(parameters);
    while (i.hasNext()) {
        i.next();
         qDebug() << ui->treeWidget->isFirstItemColumnSpanned(ui->treeWidget->itemAt(0,0));
        this->addRootTree(i.key(),i.value().filter(regExp), ui->treeWidget);
        qDebug() << "filter data" << i.value().filter(regExp);
    }
    //ui->treeWidget->blockSignals(false);
    qDebug() << "connect";
}

void parameterTemporalSeries::onShowUnits(bool click){
    QStringList headers;
    if(click){
        headers << "Parameters" << "Value" << "Unit";
        QMapIterator<QString,QStringList> i(parameters);
        QStringList list;
        while (i.hasNext()) {
            i.next();
            list=i.value();
            QString itemName;
            for(int j =0;j<list.size();++j){
                itemName=i.key() + "/" + list.at(j);
                qDebug()<<"Item Name";
                qDebug()<<itemName;
                if(plot->selectedItems.contains(itemName)){
                    qDebug()<<"Containsss";
                    qDebug()<<itemName;
                  QTreeWidgetItem* itemList = plot->items[itemName];
                  itemList->setText(2,"float");
               }
            }
        }
        ui->treeWidget->setColumnCount(3);
    }else{
        headers << "Parameters" << "Value";
        ui->treeWidget->setColumnCount(2);
    }
    ui->treeWidget->setHeaderLabels(headers);
}

void parameterTemporalSeries::onStopButton(){
    if(plot->stopPressed)
      plot->stopPressed = false;
    else
      plot->stopPressed = true;
}

parameterTemporalSeries::~parameterTemporalSeries()
{
    delete ui;
}
