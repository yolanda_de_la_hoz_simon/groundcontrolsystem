/*
  connection
  @author  Yolanda de la Hoz Simón
  @date    03-2015
  @version 1.0
*/

#include "../include/ground_control_system/connection.hpp"
#include "../../ground_control_system-build/ui_connection.h"
#include <iostream>
using namespace std;

Connection::Connection(QWidget *parent, Collector* collector,UserCommander* usercommander) :
    QDialog(parent),
    ui(new Ui::connection)
{
    ui->setupUi(this);
    listener=collector;
    writer=usercommander;
}

void Connection::showNoMasterMessage() {
    QMessageBox msgBox;
    msgBox.setText("Couldn't find the ros master.");
    msgBox.exec();
    close();
}

/*
 * These triggers whenever the button is clicked, regardless of whether it
 * is already checked or not.
 */

void Connection::on_button_connect_clicked(bool check ) {
    if ( ui->checkbox_use_environment->isChecked() ) {
        if ( !listener->init() ) {
            showNoMasterMessage();
        } else {
            ui->connectButton->setEnabled(false);
        }
    } else {
        try{
            listener->init(ui->line_edit_master->text().toStdString(),ui->line_edit_host->text().toStdString());
            writer->init(ui->line_edit_master->text().toStdString(),ui->line_edit_host->text().toStdString());

            ui->connectButton->setEnabled(false);
            ui->line_edit_master->setReadOnly(true);
            ui->line_edit_host->setReadOnly(true);
        }catch(int e){
            showNoMasterMessage();
            cout << "Unable to find rosmaster node" << e << '\n';

        }
    }
}

void Connection::on_checkbox_use_environment_stateChanged(int state) {
    bool enabled;
    if ( state == 0 ) {
        enabled = true;
    } else {
        enabled = false;
    }
    ui->line_edit_master->setEnabled(enabled);
    ui->line_edit_host->setEnabled(enabled);
}
void Connection::close()
{
    this->~Connection();
}
Connection::~Connection()
{
    delete ui;
}

