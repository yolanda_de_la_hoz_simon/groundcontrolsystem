#include "rviz/visualization_manager.h"
#include "rviz/visualization_frame.h"
#include "rviz/render_panel.h"
#include "rviz/display.h"
#include "rviz/yaml_config_reader.h"
#include "rviz/yaml_config_writer.h"
#include "rviz/view_manager.h"
#include "rviz/panel_dock_widget.h"

#include "../include/ground_control_system/sphereview.hpp"
#include "../../ground_control_system-build/ui_sphereview.h"

sphereView::sphereView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::sphereView)
{
    ui->setupUi(this);

    frame_=new rviz::VisualizationFrame();
    frame_->setSplashPath("");
    frame_->initialize();

    render_panel_ = new rviz::RenderPanel();
    /*rviz::YamlConfigReader reader;
    rviz::Config config;
     std::string filename = "/home/yolanda/save2/groundcontrolsystem/src/uav3d/sphere.rviz";
            //"/home/yolanda/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack/stack/HMI/droneSimulatorRvizROSModule/launch/Simulator_rviz.rviz" ;
    // /home/yolanda/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack/stack/HMI/droneSimulatorRvizROSModule/launch/Simulator_rviz.rviz
    //home/yolanda/iarc_ws/src/scripts/perception.rviz
   // /home/yolanda/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack/stack/HMI/droneSimulatorRvizROSModule/launch/Simulator_rviz.rviz
    reader.readFile( config, QString::fromStdString( filename ));
   /* if( !reader.error() )
    {
        std::cout << "Se ha leido el archivo config correctamente";
        frame_->load( config);
    }
*/

    frame_->initialize("/home/yolanda/iarc_ws/src/scripts/sphere.rviz");
    manager_=frame_->getManager();
    render_panel_=manager_->getRenderPanel();
    // Set the top-level layout for Perception View widget.
    ui->dockWidget->setWidget( render_panel_);
}

sphereView::~sphereView()
{
    delete ui;
}
