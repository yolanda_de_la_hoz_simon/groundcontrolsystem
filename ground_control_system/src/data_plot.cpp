#include <stdlib.h>
#include <qt4/QtGui/QPrintDialog>
#include <qt4/QtGui/QPrinter>
#include <qwt/qwt_color_map.h>
#include <qwt/qwt_scale_widget.h>
#include <qwt/qwt_scale_draw.h>
#include <std_msgs/String.h>
#include <qwt/qwt_painter.h>
#include <qwt/qwt_plot_canvas.h>
#include <qwt/qwt_plot_marker.h>
#include <qwt/qwt_plot_renderer.h>
#include <qwt/qwt_plot.h>
#include <qwt/qwt_plot_grid.h>
#include <qwt/qwt_plot_curve.h>
#include <qwt/qwt_scale_widget.h>
#include <qwt/qwt_legend.h>
#include <qwt/qwt_scale_draw.h>
#include <qwt/qwt_picker.h>
#include <qwt/qwt_math.h>
#include <qwt/qwt_color_map.h>
#include <qwt/qwt_plot_spectrogram.h>
#include <qwt/qwt_scale_widget.h>
#include <qwt/qwt_scale_draw.h>
#include <qwt/qwt_plot_zoomer.h>
#include <qwt/qwt_plot_panner.h>
#include <qwt/qwt_plot_layout.h>
#include "../include/ground_control_system/data_plot.hpp"
#include <ctime>
class MyZoomer: public QwtPlotZoomer
{
public:
    MyZoomer(QwtPlotCanvas *canvas):
        QwtPlotZoomer(canvas)
    {
       setTrackerMode(AlwaysOn);
    }
};


class TimeScaleDraw: public QwtScaleDraw
{
public:
    TimeScaleDraw(const QTime &base):
        baseTime(base)
    {
    }
    virtual QwtText label(double v) const
    {
        QTime upTime = baseTime.addSecs((int)v);
        return upTime.toString();

    }
private:
    QTime baseTime;
};

DataPlot::DataPlot(QWidget *parent, Collector* collector):
    QwtPlot(parent),
    d_interval(0),
    d_timerId(-1)
{
    node=collector;
    QObject::connect( node, SIGNAL( parameterReceived( )), this, SLOT( onParameterReceived( )));

    dataCount=0;
    posicionBuffer=PLOT_SIZE;
    plotColors <<"red"<<"blue"<<"green"<<"black"<<"yellow"<<"magenta"<<"cyan"<<"gray"<<"darkCyan"<<"darkMagenta"<<"darkYellow"<<"darkGray"<<"darkRed"<<"darkBlue"<<"darkGreen"<<"lightGray";
    iteratorColors=0;
    highRateOption = false;
    stopPressed=false;
    alignScales();
    setAutoReplot(false);

    TelposX ="Telemetry/pos.X";
    TelposZ ="Telemetry/pos.Z";
    TelposY ="Telemetry/pos.Y";
    EstposX ="Ext.Kallman Filter/pos.X";
    EstposZ ="Ext.Kallman Filter/pos.Z";
    EstposY ="Ext.Kallman Filter/pos.Y";

    // LeftButton for the zooming
    // MidButton for the panning
    // RightButton: zoom out by 1
    // Ctrl+RighButton: zoom out to full size

    QwtPlotZoomer* zoomer = new MyZoomer(canvas());
#if QT_VERSION < 0x040000
    zoomer->setMousePattern(QwtEventPattern::MouseSelect2,
        Qt::RightButton, Qt::ControlButton);
#else
    zoomer->setMousePattern(QwtEventPattern::MouseSelect2,
        Qt::RightButton, Qt::ControlModifier);
#endif
    zoomer->setMousePattern(QwtEventPattern::MouseSelect3,
        Qt::RightButton);

    QwtPlotPanner *panner = new QwtPlotPanner(canvas());
    panner->setAxisEnabled(QwtPlot::yRight, false);
    panner->setMouseButton(Qt::MidButton);

    // Avoid jumping when labels with more/less digits
    // appear/disappear when scrolling vertically

    const QFontMetrics fm(axisWidget(QwtPlot::yLeft)->font());
    QwtScaleDraw *sd = axisScaleDraw(QwtPlot::yLeft);
    sd->setMinimumExtent( fm.width("100.00") );

    const QColor c(Qt::darkBlue);
    zoomer->setRubberBandPen(c);
    zoomer->setTrackerPen(c);





    setGridPlot();


    initTimeData();

#if 0
    //  Insert zero line at y = 0
    QwtPlotMarker *mY = new QwtPlotMarker();
    mY->setLabelAlignment(Qt::AlignRight|Qt::AlignTop);
    mY->setLineStyle(QwtPlotMarker::HLine);
    mY->setYValue(0.0);
    mY->attach(this);
#endif


    initAxisX();

    initAxisY();

}
// Init X axis.
void DataPlot::setGridPlot()
{
    QwtPlotGrid *grid = new QwtPlotGrid;
    grid->enableXMin(true);
    grid->enableYMin(true);
    grid->setMajPen(QPen(Qt::black, 0, Qt::DotLine));
    grid->setMinPen(QPen(Qt::gray, 0 , Qt::DotLine));
    grid->attach(this);
}

void DataPlot::initTimeData()
{
   //  Initialize data
   for (int i = 0; i< PLOT_SIZE; i++)
   {
      d_x[PLOT_SIZE- 1 - i] = i;// time axis
   }

   // Erase memory blocks
   for (int i = 0; i< PLOT_SIZE; i++)
   {
     param6[i] = 0;
     param3[i] = 0;
     param10[i] = 0;
     param11[i] = 0;
     param12[i] = 0;
     param13[i] = 0;

   }

}


// Init X axis.
void DataPlot::initAxisX()
{
    QwtText xTitle("System Uptime [h:m:s]");
    xTitle.setFont(QFont("Ubuntu", 10));
    setAxisTitle(QwtPlot::xBottom, xTitle);
    setAxisScaleDraw(QwtPlot::xBottom,
        new TimeScaleDraw(upTime()));
    setAxisScale(QwtPlot::xBottom, 0, PLOT_SIZE );
    setAxisLabelRotation(QwtPlot::xBottom, -50.0);
    setAxisLabelAlignment(QwtPlot::xBottom, Qt::AlignLeft | Qt::AlignBottom);
    QwtScaleWidget *scaleWidget = axisWidget(QwtPlot::xBottom);
    const int fmh = QFontMetrics(scaleWidget->font()).height();
    scaleWidget->setMinBorderDist(0, fmh / 2);
}

// Init Y axis.
void DataPlot::initAxisY()
{
    QwtText yTitle("Values");
    yTitle.setFont(QFont("Ubuntu", 10));
    setAxisTitle(QwtPlot::yLeft,yTitle);
    setAxisScale(QwtPlot::yLeft, -1, 20);
    curves.insert(TelposX,new QwtPlotCurve(TelposX));
    curves.insert(TelposY,new QwtPlotCurve(TelposY));
    curves.insert(TelposZ,new QwtPlotCurve(TelposZ));
    curves.insert(EstposX,new QwtPlotCurve(EstposX));
    curves.insert(EstposY,new QwtPlotCurve(EstposY));
    curves.insert(EstposZ,new QwtPlotCurve(EstposZ));
    setTimerInterval(1000);// 1 second = 1000
}

QTime DataPlot::upTime() const
{
    // Create a raw time_t variable and a tm structure
    time_t rawtime;
    struct tm * timeinfo;
    // Get the current time and place it in time_t
    time ( &rawtime );
    // Get the locatime from the time_t and put it into our structure timeinfo
    timeinfo = localtime ( &rawtime );
    // Now we have access to hours, minutes, seconds etc as member variables of all type int
    int hour = timeinfo->tm_hour;
    int min = timeinfo->tm_min;
    int sec = timeinfo->tm_sec;
    // Just print out the hours and minutes to show you
    qDebug() << "System Uptime" << "=" << hour << ":" << min << ":" << sec << endl;
    int time = hour*3600+min*60+sec-60;

    QTime t;
        t = t.addSecs(time);
    return t;
}

// item identifier and treeWidgetItem structure
void DataPlot::assignColorIcon(QString id,QTreeWidgetItem* item)
{
    QPixmap pixmap(15,40);
    if(colors[id]==NULL){
        QString color= plotColors.at(iteratorColors);
        iteratorColors++;
        colors.insert(id,color);
    }
    pixmap.fill(QColor(colors[id]));
    QIcon icon(pixmap);
    item->setIcon(0, icon);
}

// item identifier and treeWidgetItem structure
void DataPlot::eraseColorIcon(QString id,QTreeWidgetItem* item)
{
    QPixmap pixmap(15,40);
    pixmap.fill(QColor("white"));
    QIcon whiteIcon(pixmap);
    iconChange.insert(id,2);
    item->setIcon(0, whiteIcon);
}

// Hash colors -> text, color.
// Hash curves -> text, curves.
// Hash iconChange -> text, iconChange. iconChange flags: 0(ChangeWhite),1(ChangeColor),3(notChange)
void DataPlot::clickToPlot(QTreeWidgetItem* item, int colum)
{
    if(colum==0){ // handle only signals in colum 0
        QString text = item->text(colum);
        qDebug() << text; // the item is checked, insert in checked items.
        QString id;
        if(item->parent()!=NULL){
            id = item->parent()->text(0) + "/" + text;
            qDebug()<< id;}else
            id=item->text(colum);
        if(!selectedItems.contains(id)){ // if item is not in checked items.
            qDebug()<< "insert";
            selectedItems << id;
            qDebug()<< selectedItems;
            items.insert(id,item);
            iconChange.insert(id,1);
            assignColorIcon(id,item);
            // if(curves[id]==NULL){
            //}
        }else{// if item is in list checked items. detach plot and icon white.
            if(iconChange[id]==NULL){ //notChange.
                qDebug()<< "detach curve";
                qDebug()<< id;
                curves[id]->setVisible(false);
                qDebug()<< "remove";
                eraseColorIcon(id,item);
                selectedItems.removeAll(id);
                items[id]->setText(1,"");
            }
        }
        //Restore flags
        iconChange[id]=NULL;
    }

}


//
//  Set a plain canvas frame and align the scales to it
//
void DataPlot::alignScales()
{
    // The code below shows how to align the scales to
    // the canvas frame, but is also a good example demonstrating
    // why the spreaded API needs polishing.

    canvas()->setFrameStyle(QFrame::Box | QFrame::Plain );
    canvas()->setLineWidth(1);

    for ( int i = 0; i < QwtPlot::axisCnt; i++ )
    {
        QwtScaleWidget *scaleWidget = (QwtScaleWidget *)axisWidget(i);
        if ( scaleWidget )
            scaleWidget->setMargin(0);

        QwtScaleDraw *scaleDraw = (QwtScaleDraw *)axisScaleDraw(i);
        if ( scaleDraw )
            scaleDraw->enableComponent(QwtAbstractScaleDraw::Backbone, false);
    }
}

void DataPlot::setTimerInterval(double ms)
{
    d_interval = qRound(ms);

    if ( d_timerId >= 0 )
    {
        killTimer(d_timerId);
        d_timerId = -1;
    }
    if (d_interval >= 0 )
        d_timerId = startTimer(d_interval);
}

// Read incoming connections
void DataPlot::timerEvent(QTimerEvent *e)
{
    // fill the values.
    for ( int i = dataCount; i > 0; i-- )
    {
      param6[i] = param6[i-1];
      param3[i] = param3[i-1];
      param2[i] = param2[i-1];
      param11[i] = param11[i-1];
      param12[i] = param12[i-1];
      param13[i] = param13[i-1];

    }

    QVariant var;

    param6[0] = node->posx;// we put the last value
    msgs.insert(TelposX,param6);
    if(selectedItems.contains(TelposX)&&items[TelposX]!=NULL){
        var.setValue(node->posx);
        curves[TelposX]->setPen(QPen(colors[TelposX]));
        curves[TelposX]->setRawSamples(d_x,msgs[TelposX],dataCount);
        curves[TelposX]->attach(this);
        curves[TelposX]->setVisible(true);
        items[TelposX]->setText(1,var.toString());
     }


    QVariant var2;

    param3[0] = node->posz;
    msgs.insert(TelposZ,param3);
    if(selectedItems.contains(TelposZ)&&items[TelposZ]!=NULL){
        var2.setValue(node->posz);
        curves[TelposZ]->setPen(QPen(colors[TelposZ]));
        curves[TelposZ]->setRawSamples(d_x,msgs[TelposZ],dataCount);
        curves[TelposZ]->attach(this);
        curves[TelposZ]->setVisible(true);
        items[TelposZ]->setText(1,var2.toString());
    }





    QVariant var3;

    param2[0] = node->posy;
    msgs.insert(TelposY,param2);
    if(selectedItems.contains(TelposY)&&items[TelposY]!=NULL){
        var3.setValue(node->posy);
        curves[TelposY]->setPen(QPen(colors[TelposY]));
        curves[TelposY]->setRawSamples(d_x,msgs[TelposY],dataCount);
        curves[TelposY]->attach(this);
        curves[TelposY]->setVisible(true);
        items[TelposY]->setText(1,var3.toString());
    }


    QVariant var4;

    param11[0] = node->e1posx;// we put the last value
    msgs.insert(EstposX,param11);
    if(selectedItems.contains(EstposX)){
        var4.setValue(node->e1posx);
        curves[EstposX]->setPen(QPen(colors[EstposX]));
        curves[EstposX]->setRawSamples(d_x,msgs[EstposX],dataCount);
        curves[EstposX]->attach(this);
        curves[EstposX]->setVisible(true);
        items[EstposX]->setText(1,var4.toString());

    }
    if( items[EstposX]!=NULL)
        items[EstposX]->setText(1,var4.toString());


    QVariant var5;

    param12[0] = node->e1posz;
    msgs.insert(EstposZ,param12);
    if(selectedItems.contains(EstposZ)){
        var5.setValue(node->e1posz);
        curves[EstposZ]->setPen(QPen(colors[EstposZ]));
        curves[EstposZ]->setRawSamples(d_x,msgs[EstposZ],dataCount);
        curves[EstposZ]->attach(this);
        curves[EstposZ]->setVisible(true);
        items[EstposZ]->setText(1,var.toString());
    }
    if( items[EstposZ]!=NULL)
        items[EstposZ]->setText(1,var5.toString());


    QVariant var6;

    param13[0] = node->e1posy;
    msgs.insert(EstposY,param13);
    if(selectedItems.contains(EstposY)){
        var6.setValue(node->e1posy);
        curves[EstposY]->setPen(QPen(colors[EstposY]));
        curves[EstposY]->setRawSamples(d_x,msgs[EstposY],dataCount);
        curves[EstposY]->attach(this);
        curves[EstposY]->setVisible(true);
        items[EstposY]->setText(1,var.toString());
    }
    if( items[EstposY]!=NULL)
        items[EstposY]->setText(1,var6.toString());


    for ( int j = 0; j < PLOT_SIZE; j++ )
       d_x[j]++;

    setAxisScale(QwtPlot::xBottom,
         d_x[PLOT_SIZE - 1],d_x[0]);

    if(dataCount<PLOT_SIZE)
        dataCount++;

    if(!stopPressed)
    replot();

}

// Read incoming connections
// Fill the buffer.
void DataPlot::onParameterReceived()
{


    QVariant var;
    if(selectedItems.contains(TelposX))
        var.setValue(node->posx);
    if( items[TelposX]!=NULL)
        items[TelposX]->setText(1,var.toString());

    QVariant var2;
    if(selectedItems.contains(TelposZ))
        var2.setValue(node->posz);
    if( items[TelposZ]!=NULL)
        items[TelposZ]->setText(1,var2.toString());


    QVariant var3;
    if(selectedItems.contains(TelposY))
        var3.setValue(node->posy);
    if( items[TelposY]!=NULL)
        items[TelposY]->setText(1,var3.toString());


    QVariant var4;
    if(selectedItems.contains(EstposX))
        var4.setValue(node->e1posx);
    if( items[EstposX]!=NULL)
        items[EstposX]->setText(1,var4.toString());


    QVariant var5;
    if(selectedItems.contains(EstposZ))
        var5.setValue(node->e1posz);
    if( items[EstposZ]!=NULL)
        items[EstposZ]->setText(1,var5.toString());


    QVariant var6;
    if(selectedItems.contains(EstposY))
        var6.setValue(node->e1posy);
    if( items[EstposY]!=NULL)
        items[EstposY]->setText(1,var6.toString());

}

void DataPlot::resizeAxisXScale(int ms)
{
    setAxisScale(QwtPlot::xBottom, 0, ms);
    replot();
}
void DataPlot::resizeAxisYScale(int ms)
{
    setAxisScale(QwtPlot::yLeft, -1,ms);
    replot();
}
// TODO
void DataPlot::saveAsSVG()
{
#if 1
    QPrinter printer;
#else
    QPrinter printer(QPrinter::HighResolution);
#endif
    printer.setOrientation(QPrinter::Landscape);
    printer.setOutputFileName("iarc_simulation01.pdf");
    QPrintDialog dialog(&printer);
    if ( dialog.exec() )
    {
        QwtPlotRenderer renderer;

        renderer.setDiscardFlag(QwtPlotRenderer::DiscardBackground, false);
        renderer.setLayoutFlag(QwtPlotRenderer::KeepFrames, true);

        renderer.renderTo(this, printer);
    }
}
