/*
  uercommander
  Launch a ROS node to publish topics.
  @author  Yolanda de la Hoz Simón
  @date    03-2015
  @version 1.0
*/

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <ros/network.h>
#include <string>
#include <sstream>
#include "../include/ground_control_system/usercommander.hpp"


/*****************************************************************************
** Implementation
*****************************************************************************/

 UserCommander:: UserCommander(int argc, char** argv ) :
    init_argc(argc),
    init_argv(argv)
    {}

 UserCommander::~ UserCommander() {
    if(ros::isStarted()) {
      ros::shutdown(); // Kill all open subscriptions, publications, service calls, and service servers.
      ros::waitForShutdown();
    }
    wait();
}

bool UserCommander::init() {
    ros::init(init_argc,init_argv,"GroundControlSystem");// ros node started.
    if ( ! ros::master::check() ) {// Check if roscore has been initialized.
        return false;
    }
    ros::start(); // explicitly needed since our nodehandle is going out of scope.
    ros::NodeHandle n;
    // Subscribe ros topics here.
    gcs_publisher = n.advertise<common::UserCommands>("User_Commands", 1000);
    start();
    return true;
}

// Connect with a given master url and host url
bool UserCommander::init(const std::string &master_url, const std::string &host_url) {
    std::map<std::string,std::string> remappings;
    remappings["__master"] = master_url;
    remappings["__hostname"] = host_url;
    ros::init(remappings,"GroundControlSystem");
    if ( ! ros::master::check() ) {
        return false;
    }
    ros::start(); // explicitly call to ros start
    ros::NodeHandle n;
    // Topic communications
    // Publish a Topic called "publisher"
    gcs_publisher = n.advertise<common::UserCommands>("User_Commands", 1000);
    start();
    return true;
}

void  UserCommander::run() {
    ros::Rate loop_rate(1);
    int count = 0;
    while ( ros::ok() ) {
        //msg.type=order;
        gcs_publisher.publish(msg);
       // log(Info,std::string("Ground Control System sent: ")+msg.type);
        ros::spinOnce();
        loop_rate.sleep();
        ++count;
    }
    std::cout << "Ros shutdown, proceeding to close the gui." << std::endl;
    Q_EMIT rosShutdown(); // used to signal the gui for a shutdown (useful to roslaunch)
}


void  UserCommander::log( const LogLevel &level, const std::string &msg) {
    logging_model.insertRows(logging_model.rowCount(),1);
    std::stringstream logging_model_msg;
    switch ( level ) {
        case(Debug) : {
                ROS_DEBUG_STREAM(msg);
                logging_model_msg << "[DEBUG] [" << ros::Time::now() << "]: " << msg;
                break;
        }
        case(Info) : {
                ROS_INFO_STREAM(msg);
                logging_model_msg << "[INFO] [" << ros::Time::now() << "]: " << msg;
                break;
        }
        case(Warn) : {
                ROS_WARN_STREAM(msg);
                logging_model_msg << "[INFO] [" << ros::Time::now() << "]: " << msg;
                break;
        }
        case(Error) : {
                ROS_ERROR_STREAM(msg);
                logging_model_msg << "[ERROR] [" << ros::Time::now() << "]: " << msg;
                break;
        }
        case(Fatal) : {
                ROS_FATAL_STREAM(msg);
                logging_model_msg << "[FATAL] [" << ros::Time::now() << "]: " << msg;
                break;
        }
    }
    QVariant new_row(QString(logging_model_msg.str().c_str()));
    logging_model.setData(logging_model.index(logging_model.rowCount()-1),new_row);
    Q_EMIT loggingUpdated(); // used to readjust the scrollbar
}


