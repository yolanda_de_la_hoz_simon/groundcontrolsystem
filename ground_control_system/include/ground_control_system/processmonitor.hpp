#ifndef PROCESSMONITOR_H
#define PROCESSMONITOR_H

#include <QWidget>
#include <QDockWidget>
#include <QTreeWidgetItem>
#include <QListWidgetItem>
#include <QTreeWidget>
#include <QString>


namespace Ui {
class processMonitor;
}

class processMonitor : public QWidget
{
    Q_OBJECT
    void initTree(QMap<QString, QStringList> algorithmsList, QTreeWidget *tree);
    void initParameterList(QStringList list, QTreeWidget *tree);
    void addRootTree(QString name, QStringList list, QTreeWidget *tree);
    void addChildTree(QTreeWidgetItem *parent, QStringList list, QString description);
    void setSignalHandlers();
public:
    explicit processMonitor(QWidget *parent = 0);
    ~processMonitor();
public Q_SLOTS:
   // void onTextFilterChange(const QString &arg1);

private:
    Ui::processMonitor *ui;
    QMap<QString,QStringList> processList;

};

#endif // PROCESSMONITOR_H
