/**
 * @file /include/GroundControlSystem/usercommander.hpp
 *
 * @brief ROS node thread to publish topics.
 *
 * @date March 2015
 **/
/*****************************************************************************
** Ifdefs
*****************************************************************************/

#ifndef USERCOMMANDER_H
#define USERCOMMANDER_H


/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <string>
#include <std_msgs/String.h>
#include "common/UserCommands.h"
#include <QThread>
#include <QStringListModel>


/*****************************************************************************
** Class
*****************************************************************************/

class UserCommander : public QThread {
    Q_OBJECT
public:
	UserCommander(int argc, char** argv );
	virtual ~UserCommander();
	bool init();
        std::string order;
        common::UserCommands msg;

	bool init(const std::string &master_url, const std::string &host_url);
	void run();

	/*********************
	** Logging
	**********************/
	enum LogLevel {
	         Debug,
	         Info,
	         Warn,
	         Error,
	         Fatal
	 };

	QStringListModel* loggingModel() { return &logging_model; }
	void log( const LogLevel &level, const std::string &msg);

Q_SIGNALS:
	void loggingUpdated();
        void rosShutdown();

private:
	int init_argc;
	char** init_argv;
	ros::Publisher gcs_publisher;
        QStringListModel logging_model;
};


#endif // USERCOMMANDER_H

