#ifndef SPHEREVIEW_H
#define SPHEREVIEW_H

#include <QWidget>
#include <QDockWidget>
namespace Ui {
class sphereView;
}
namespace rviz
{
class Display;
class RenderPanel;
class VisualizationManager;
class ViewManager;
class VisualizationFrame;
}
class sphereView : public QWidget
{
    Q_OBJECT

public:
    explicit sphereView(QWidget *parent = 0);
    ~sphereView();

private:
    Ui::sphereView *ui;
    rviz::VisualizationManager* manager_;
    rviz::ViewManager* view_man;
    rviz::RenderPanel* render_panel_;
    rviz::VisualizationFrame* frame_;
    rviz::Display* grid_;
};

#endif // SPHEREVIEW_H



