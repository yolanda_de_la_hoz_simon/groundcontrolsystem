#ifndef _DATA_PLOT_H
#define _DATA_PLOT_H 1

#include <qwt_plot.h>
#include <qwt/qwt_plot_curve.h>
#include <QTreeWidgetItem>
#include <QStringList>
#include <QHash>
#include <QHashIterator>
#include <QList>
#include <QTime>
#include "collector.hpp"

const int PLOT_SIZE =  60;      // 0 to 200
const int HIGHRATE_SIZE = 20;      // 0 to 200


class DataPlot : public QwtPlot
{
    Q_OBJECT

public:
    DataPlot(QWidget* = NULL, Collector *collector=0);
    void alignScales();
    QStringList selectedItems;
    QStringList plotColors;
    int iteratorColors;
    bool iconColorChange; // make difference between change by checkbox and icons.
    bool iconWhiteChange;
    bool stopPressed;
    int dataCount;
    bool highRateOption;
    QHash<QString,QwtPlotCurve*> curves;
    QHash<QString,QString> colors;
    QHash<QString,int> iconChange;
    QHash<QString,double*> msgs;
    QHash<QString,QTreeWidgetItem*> items;
    QString TelposX;
    QString TelposZ;
    QString TelposY;
    QString EstposX;
    QString EstposZ;
    QString EstposY;

    QTime upTime() const;
    void assignColorIcon(QString id,QTreeWidgetItem* item);
    void eraseColorIcon(QString id,QTreeWidgetItem* item);
    void initAxisY();
    void initAxisX();
    void initTimeData();
    void setGridPlot();

public Q_SLOTS:
    void setTimerInterval(double interval);
    void clickToPlot(QTreeWidgetItem* item, int colum);
    void resizeAxisXScale(int ms);
    void resizeAxisYScale(int ms);
    void saveAsSVG();
    void onParameterReceived();
  //  void onParameterReceived(QTimerEvent *e);

protected:
    virtual void timerEvent(QTimerEvent *e);

private:
    double d_x[PLOT_SIZE];
    double d_y[PLOT_SIZE];
    double d_z[PLOT_SIZE];
    double param2[PLOT_SIZE];
    double param4[PLOT_SIZE];
    double param5[PLOT_SIZE];
    double param6[PLOT_SIZE];
    double param7[PLOT_SIZE];
    double param8[PLOT_SIZE];
    double param3[PLOT_SIZE];
    double param10[PLOT_SIZE];
    double param11[PLOT_SIZE];
    double param12[PLOT_SIZE];
    double param13[PLOT_SIZE];

    double hrateBuff1[HIGHRATE_SIZE];
    double hrateBuff2[HIGHRATE_SIZE];
    double hrateBuff3[HIGHRATE_SIZE];
    double hrateBuff4[HIGHRATE_SIZE];
    double hrateBuff5[HIGHRATE_SIZE];
    double hrateBuff6[HIGHRATE_SIZE];
    Collector* node;


    int posicionBuffer;
    int d_interval; // timer in ms
    int d_timerId;
};

#endif
