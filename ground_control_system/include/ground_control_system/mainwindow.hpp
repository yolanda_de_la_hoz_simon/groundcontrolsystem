#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include "connection.hpp"
#include "perceptionview.hpp"
#include "collector.hpp"
#include "usercommander.hpp"
#include "parametertemporalseries.hpp"
#include "dinamicsview.hpp"
#include "processmonitor.hpp"
#include "sphereview.hpp"
namespace Ui {
class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(int argc, char** argv,QWidget *parent = 0);
    ~MainWindow();



public Q_SLOTS:
    void onStartButton();
    void onHoverButton();
    void onEmergencyStopButton();
    void onLandButton();
    void onControlModeChange(QString mode);
    void onYawZeroButton();
    void on_actionNew_connection_triggered();
    void on_action3D_Perception_View_triggered();
    void on_actionParameter_Temporal_Series_triggered();
    void on_actionAbout_Ground_Control_System_triggered();
    void on_actionContents_triggered();
    void on_actionUser_Commands_Manual_triggered();
    void close();

private:
    Ui::MainWindow *ui;
    PerceptionView *percept;
    dinamicsView *dinamic;
    sphereView *sphere;
    Connection *connection;
    processMonitor * processView;
    parameterTemporalSeries *paramPlot;
    Collector collector;
    UserCommander usercommander;

};

#endif // MAINWINDOW_H
