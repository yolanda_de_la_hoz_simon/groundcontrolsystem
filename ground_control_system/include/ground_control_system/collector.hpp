/**
 * @file /include/GroundControlSystem/collector.hpp
 *
 * @brief ROS node thread to subscribe topics.
 *
 * @date March 2015
 **/
/*****************************************************************************
** Ifdefs
*****************************************************************************/

#ifndef GroundControlSystem_COLLECTOR_HPP_
#define GroundControlSystem_COLLECTOR_HPP_

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <string>
#include "std_msgs/String.h"
#include "std_msgs/Float32MultiArray.h"
#include "geometry_msgs/Point.h"
#include "common/PerceivedState.h"
#include "common/ActionConfirmation.h"
#include "common/PoseChanges.h"
#include "common/SimulatedState.h"
#include "common/UAVState.h"
#include <QThread>
#include <QStringListModel>


/*****************************************************************************
** Class
*****************************************************************************/

class Collector : public QThread {
    Q_OBJECT
public:
	Collector(int argc, char** argv );
	virtual ~Collector();
	bool init();
	bool init(const std::string &master_url, const std::string &host_url);
        void run();
        float posx;
        float posy;
        float posz;
        float direction;
        float accelx;
        float accely;
        float accelz;
        float e1posx;
        float e1posy;
        float e1posz;
        float e2posx;
        float e2posy;
        float e2posz;
        float e3posx;
        float e3posy;
        float e3posz;
        float e4posx;
        float e4posy;
        float e4posz;
	/*********************
	** Logging
	**********************/
	enum LogLevel {
	         Debug,
	         Info,
	         Warn,
	         Error,
	         Fatal
	 };

	QStringListModel* loggingModel() { return &logging_model; }
	void log( const LogLevel &level, const std::string &msg);


Q_SIGNALS:
	void loggingUpdated();
        void rosShutdown();
        void parameterReceived();


private:

	int init_argc;
        int real_time;
	char** init_argv;
        ros::Subscriber SimulatedStateSubs;
        ros::Subscriber PoseChangesSubs;
        ros::Subscriber PerceivedStateError1Subs;
        ros::Subscriber PerceivedStateError2Subs;
        ros::Subscriber PerceivedStateError3Subs;
        ros::Subscriber PerceivedStateError4Subs;
        void simulatedStateCallback(const common::SimulatedState::ConstPtr& msg);
        void poseChangesCallback(const common::PoseChanges::ConstPtr& msg);
        void perceivedStateError1Callback(const common::PerceivedState::ConstPtr& msg);
        void perceivedStateError2Callback(const common::PerceivedState::ConstPtr& msg);
        void perceivedStateError3Callback(const common::PerceivedState::ConstPtr& msg);
        void perceivedStateError4Callback(const common::PerceivedState::ConstPtr& msg);
        QStringListModel logging_model;
};


#endif /* GroundControlSystem_QNODE_HPP_ */
