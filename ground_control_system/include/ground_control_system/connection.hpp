#ifndef CONNECTION_H
#define CONNECTION_H

#include <QDialog>
#include <QMessageBox>
#include "collector.hpp"
#include "usercommander.hpp"



namespace Ui {
class connection;
}

class Connection : public QDialog
{
    Q_OBJECT

public:
    explicit Connection(QWidget *parent = 0, Collector *collector=0, UserCommander *usercommander=0);
    ~Connection();
        void ReadSettings(); // Load up program settings at startup
        void WriteSettings(); // Save program settings when closing

        void showNoMasterMessage();
        void close();
public Q_SLOTS:
	/******************************************
	** Auto-connections (connectSlotsByName())
	*******************************************/
        void on_button_connect_clicked(bool check );
        void on_checkbox_use_environment_stateChanged(int state);

    /******************************************
    ** Manual connections
    *******************************************/
    //void updateLoggingView();
private:
    Ui::connection *ui;
    Collector* listener;
    UserCommander* writer;

};

#endif // CONNECTION_H
