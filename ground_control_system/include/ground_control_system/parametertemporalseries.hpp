#ifndef PARAMETERTEMPORALSERIES_H
#define PARAMETERTEMPORALSERIES_H

#include <QDockWidget>
#include <QTreeWidgetItem>
#include <QListWidgetItem>
#include <QTreeWidget>
#include <QString>
#include "data_plot.hpp"
#include "collector.hpp"

namespace Ui {
class parameterTemporalSeries;
}

class parameterTemporalSeries : public QDockWidget
{
    Q_OBJECT

    void initTree(QMap<QString, QStringList> algorithmsList, QTreeWidget *tree);
    void initParameterList(QStringList list, QTreeWidget *tree);
    void addRootTree(QString name, QStringList list, QTreeWidget *tree);
    void addChildTree(QTreeWidgetItem *parent, QStringList list, QString description);
    void setSignalHandlers();
public:
    explicit parameterTemporalSeries(QWidget *parent = 0, Collector *collector=0);
    void resizeEvent(QResizeEvent * event);
    ~parameterTemporalSeries();
public Q_SLOTS:
    void onTextFilterChange(const QString &arg1);
    void onShowUnits(bool click);
    void onStopButton();
private:
    Ui::parameterTemporalSeries *ui;
    DataPlot *plot;
    QStringList paramsTelemetry;
    QMap<QString,QStringList> parameters;
};

#endif // PARAMETERTEMPORALSERIES_H
