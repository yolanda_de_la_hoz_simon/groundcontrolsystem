#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import TransformStamped
from std_msgs.msg import Header
from geometry_msgs.msg import Transform
from geometry_msgs.msg import Quaternion
from geometry_msgs.msg import Vector3
from tf.msg import tfMessage
import time
import rospy
import tf
import geometry_msgs.msg
import math
import random
import roslib; roslib.load_manifest( 'rviz_plugin_tutorials' )
from sensor_msgs.msg import Imu
import rospy
from common.msg import Orders
from math import cos, sin
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from sensor_msgs.msg import JointState

a=0
b=0
c=0
padre='base_footprint'

def averquepasa(dato):
    global a
    global b
    global c
    global padre

    if dato.type == "up" and a>-45:
        a-=1

    if dato.type == "down" and a<45:
        a+=1


    if dato.type == "right" and b<45:
        b+=1

    if dato.type == "left" and b >-45:
        b-=1

    if dato.type == "q":
        c-=1

    if dato.type == "e":
        c+=1

    if dato.type == "w":
        a=0
        b=0
        c=0

def UAV3D():
    global a
    global b
    global c
    global padre

    pub = rospy.Publisher('tf', tfMessage, queue_size=10)
    rospy.init_node('UAV3D', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    contx = 0
    conty = 0

    pubText = rospy.Publisher('Scene_markers', Marker, queue_size=10)


    topic = 'test_imu'
    publisher = rospy.Publisher( topic, Imu , queue_size=10)
    br = tf.TransformBroadcaster()
    radius = 1
    angle = 0
    dist = 7

    subs = rospy.Subscriber('Orders', Orders, averquepasa)

    pub2 = rospy.Publisher('joint_states', JointState, queue_size=10)

    while not rospy.is_shutdown():


        #TEXTO

        textView = Marker()
        textView.header.frame_id = 'base_general'
        textView.type = textView.SPHERE
        textView.action = textView.ADD
        textView.ns = 'prueba'
        textView.id = 1
        textView.scale.z = 0.8
        textView.color.a = 0.0
        textView.color.r = 0.0
        textView.color.g = 0.0
        textView.color.b = 0.0
        textView.pose.position.x, textView.pose.position.y, textView.pose.position.z = (0,0,0)
        textView.scale.x, textView.scale.y, textView.scale.z =(1,1,1)
        textView.frame_locked = True
        pubText.publish(textView)

        imu = Imu()
        imu.header.frame_id = "/base_link"
        imu.header.stamp = rospy.Time.now()
        publisher.publish( imu )
        br.sendTransform((0, 0, 0),
        tf.transformations.quaternion_from_euler(0, 0, c*360),
        rospy.Time.now(),
        "base_link",
        "map")
        rate.sleep()



        #YAW
        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'base_link'
        m.child_frame_id = 'aux_yaw'
        m.transform.translation.z = -0.083

        t1 = 0.0 * math.sin(math.radians(a/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(a/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 0.0 * math.sin(math.radians(b/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(b/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 1.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z

        contx = contx+m.transform.rotation.y*0.1
        conty = conty+m.transform.rotation.x*-0.1

        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)

        #YAW2
        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'aux_yaw'
        m.child_frame_id = 'aux_yaw2'
        m.transform.translation.z = 0
        m.transform.rotation.w = 1.0
        t1 = 0.0 * math.sin(math.radians(a/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(a/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 0.0 * math.sin(math.radians(b/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(b/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z

        contx = contx+m.transform.rotation.y*0.1
        conty = conty+m.transform.rotation.x*-0.1

        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)

        #YAW3
        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'aux_yaw2'
        m.child_frame_id = 'aux_yaw3'
        m.transform.translation.z = 0
        m.transform.rotation.w = 1.0
        t1 = 0.0 * math.sin(math.radians(a/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(a/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 0.0 * math.sin(math.radians(b/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(b/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z

        contx = contx+m.transform.rotation.y*0.1
        conty = conty+m.transform.rotation.x*-0.1

        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)


        #CUERPO
        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'aux_yaw3'
        m.child_frame_id = 'base_general'
        m.transform.translation.z = 0.083

        t1 = 1.0 * math.sin(math.radians(a/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(a/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 1.0 * math.sin(math.radians(b/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(b/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z

        contx = contx+m.transform.rotation.y*0.1
        conty = conty+m.transform.rotation.x*-0.1

        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)
        if not m.transform.rotation.z == 0 :
            division = math.pow(m.transform.rotation.z,2)+1
        else:
            division=1

        #FOOTPRINT
        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'base_general'
        m.child_frame_id = 'base_footprint'
        m.transform.translation.z = -0.083
        m.transform.rotation.w = 5.0
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)


        #PIERNA 1
        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'base_general'
        m.child_frame_id = 'leg_1'
        m.transform.translation.z = -0.083
        t1 = 1.0 * math.sin(math.radians(a/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(a/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 1.0 * math.sin(math.radians(b/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(b/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z

        contx = contx+m.transform.rotation.y*0.1
        conty = conty+m.transform.rotation.x*-0.1

        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)


        #PIERNA 2
        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'aux_yaw3'
        m.child_frame_id = 'leg_2'
        m.transform.translation.z = 0
        m.transform.rotation.x = 0.0
        m.transform.rotation.w = 1.0
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)

        #PIERNA 3
        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'aux_yaw3'
        m.child_frame_id = 'leg_3'
        m.transform.translation.z = 0
        m.transform.rotation.x = 0.0
        m.transform.rotation.w = 1.0
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)

        #PIERNA 4
        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'aux_yaw3'
        m.child_frame_id = 'leg_4'
        m.transform.translation.z = 0
        m.transform.rotation.x = 0.0
        m.transform.rotation.w = 1.0
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)

        rate.sleep()        

if __name__ == '__main__':
    try:
        UAV3D()
    except rospy.ROSInterruptException:
        pass
