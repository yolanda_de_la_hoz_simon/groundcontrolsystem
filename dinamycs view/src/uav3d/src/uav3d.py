#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import TransformStamped
from std_msgs.msg import Header
from geometry_msgs.msg import Transform
from geometry_msgs.msg import Quaternion
from geometry_msgs.msg import Vector3
from tf.msg import tfMessage
import time
import tf
import geometry_msgs.msg
import math
import random
import roslib
from sensor_msgs.msg import Imu
import rospy
from common.msg import Orders
from math import cos, sin
import os


a=0
b=0
c=0

def controls(dato):
    global a
    global b
    global c
    global padre

    if dato.type == "up" and a>-45:
        a-=1

    if dato.type == "down" and a<45:
        a+=1


    if dato.type == "right" and b<45:
        b+=1

    if dato.type == "left" and b >-45:
        b-=1

    if dato.type == "q":
        c+=1

    if dato.type == "e":
        c-=1

    if dato.type == "w":
        a=0
        b=0
        c=0

    if dato.type == "_":
        os.system('./lanzador.sh stop')

def UAV3D():
    global a
    global b
    global c

    pub = rospy.Publisher('tf', tfMessage, queue_size=10)
    rospy.init_node('UAV3D', anonymous=True)
    rate = rospy.Rate(10) # 10hz
 
    subs = rospy.Subscriber('Orders', Orders, controls)

    while not rospy.is_shutdown():

#AUXYAW
        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'base_link'
        m.child_frame_id = 'aux_yaw'
        m.transform.translation.z = 0

        t1 = 0.0 * math.sin(math.radians(a/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(a/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 0.0 * math.sin(math.radians(b/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(b/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 1.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)



#CUERPO
        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'aux_yaw'
        m.child_frame_id = 'base_general'
        m.transform.translation.z = 0

        t1 = 1.0 * math.sin(math.radians(a/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(a/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 1.0 * math.sin(math.radians(b/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(b/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)

        rate.sleep()        

if __name__ == '__main__':
    try:
        UAV3D()
    except rospy.ROSInterruptException:
        pass
