#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import TransformStamped
from std_msgs.msg import Header
from geometry_msgs.msg import Transform
from geometry_msgs.msg import Quaternion
from geometry_msgs.msg import Vector3
from tf.msg import tfMessage
import time
import tf
import geometry_msgs.msg
import math
import random
import roslib
import os
from common.msg import Orders
from math import cos, sin
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray

a=0
b=0
c=0

def controls(dato):
    global a
    global b
    global c
    global d

    if dato.type == "up" and a>-45:
        a-=1

    if dato.type == "down" and a<45:
        a+=1


    if dato.type == "right" and b<45:
        b+=1

    if dato.type == "left" and b >-45:
        b-=1

    if dato.type == "q":
        c+=1

    if dato.type == "e":
        c-=1

    if dato.type == "w":
        a=0
        b=0
        c=0

    if dato.type == "_":
        os.system('./lanzador.sh stop')


def UAV3D():
    global a
    global b
    global c

    pub = rospy.Publisher('tf', tfMessage, queue_size=10)
    rospy.init_node('Sphere', anonymous=True)
    rate = rospy.Rate(10) # 10hz

    pubText = rospy.Publisher('Axis_degrees', Marker, queue_size=10)

    subs = rospy.Subscriber('Orders', Orders, controls)

    while not rospy.is_shutdown():


#TEXTO Z

        textViewZ = Marker()
        textViewZ.header.frame_id = 'cone'
        textViewZ.type = textViewZ.TEXT_VIEW_FACING
        textViewZ.action = textViewZ.ADD
        textViewZ.ns = 'eje Z'
        textViewZ.id = 1
        textViewZ.color.a = 1.0
        textViewZ.color.r = 1.0
        textViewZ.color.g = 1.0
        textViewZ.color.b = 1.0
        textViewZ.pose.position.x, textViewZ.pose.position.y, textViewZ.pose.position.z = (0,0,-0.07)
        textViewZ.scale.x, textViewZ.scale.y, textViewZ.scale.z =(0.05,0.05,0.05)
        textViewZ.text = str(c%360)
        pubText.publish(textViewZ)

#TEXTO X

        textViewX = Marker()
        textViewX.header.frame_id = 'cone3'
        textViewX.type = textViewX.TEXT_VIEW_FACING
        textViewX.action = textViewX.ADD
        textViewX.ns = 'eje X'
        textViewX.id = 2
        textViewX.color.a = 1.0
        textViewX.color.r = 1.0
        textViewX.color.g = 1.0
        textViewX.color.b = 1.0
        textViewX.pose.position.x, textViewX.pose.position.y, textViewX.pose.position.z = (0,0,-0.07)
        textViewX.scale.x, textViewX.scale.y, textViewX.scale.z =(0.05,0.05,0.05)
        textViewX.text = str(a%360)
        pubText.publish(textViewX)

#TEXTO Y

        textViewY = Marker()
        textViewY.header.frame_id = 'cone2'
        textViewY.type = textViewY.TEXT_VIEW_FACING
        textViewY.action = textViewY.ADD
        textViewY.ns = 'eje Y'
        textViewY.id = 3
        textViewY.color.a = 1.0
        textViewY.color.r = 1.0
        textViewY.color.g = 1.0
        textViewY.color.b = 1.0
        textViewY.pose.position.x, textViewY.pose.position.y, textViewY.pose.position.z = (0,0,-0.07)
        textViewY.scale.x, textViewY.scale.y, textViewY.scale.z =(0.05,0.05,0.05)
        textViewY.text = str(b%360)
        pubText.publish(textViewY)

#BASE AXIS
        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'base_link'
        m.child_frame_id = 'cylinder_base'
        m.transform.translation.z = 0

        t1 = 0.0 * math.sin(math.radians(a/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(a/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 0.0 * math.sin(math.radians(b/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(b/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)

#BASE AXIS2
        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'base_link'
        m.child_frame_id = 'cylinder2_base'
        m.transform.translation.z = 0

        t1 = 1.0 * math.sin(math.radians(90/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(90/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 0.0 * math.sin(math.radians(b/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(b/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)

#BASE AXIS3
        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'base_link'
        m.child_frame_id = 'cylinder3_base'
        m.transform.translation.z = 0

        t1 = 0.0 * math.sin(math.radians(a/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(a/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 1.0 * math.sin(math.radians(90/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(90/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)

#CONE AXIS

        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'base_link'
        m.child_frame_id = 'cone_base'
        m.transform.translation.z = -0.5

        t1 = 0.0 * math.sin(math.radians(a/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(a/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 0.0 * math.sin(math.radians(b/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(b/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)

#CONE2 AXIS

        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'base_link'
        m.child_frame_id = 'cone2_base'
        m.transform.translation.x = 0.0
        m.transform.translation.y = -0.5
        m.transform.translation.z = 0.0

        t1 = 1.0 * math.sin(math.radians(270/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(270/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 0.0 * math.sin(math.radians(b/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(b/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))


        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)


#CONE3 AXIS

        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'base_link'
        m.child_frame_id = 'cone3_base'
        m.transform.translation.x = 0.5
        m.transform.translation.y = 0.0
        m.transform.translation.z = 0.0

        t1 = 0.0 * math.sin(math.radians(a/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(a/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 1.0 * math.sin(math.radians(270/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(270/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(0))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)


#SPHERE


        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'base_link'
        m.child_frame_id = 'sphere'
        m.transform.translation.z = 0

        t1 = 1.0 * math.sin(math.radians(a/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(a/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 1.0 * math.sin(math.radians(b/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(b/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 1.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)


#CYLINDER

        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'sphere'
        m.child_frame_id = 'cylinder'
        m.transform.translation.z = 0

        t1 = 0.0 * math.sin(math.radians(a/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(a/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 0.0 * math.sin(math.radians(b/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(b/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)

#CYLINDER2

        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'sphere'
        m.child_frame_id = 'cylinder2'
        m.transform.translation.z = 0

        t1 = 1.0 * math.sin(math.radians(90/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(90/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 0.0 * math.sin(math.radians(b/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(b/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)

#CYLINDER3

        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'sphere'
        m.child_frame_id = 'cylinder3'
        m.transform.translation.z = 0

        t1 = 0.0 * math.sin(math.radians(a/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(a/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 1.0 * math.sin(math.radians(90/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(90/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)

#CONE

        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'sphere'
        m.child_frame_id = 'cone'
        m.transform.translation.z = -0.5

        t1 = 0.0 * math.sin(math.radians(a/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(a/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 0.0 * math.sin(math.radians(b/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(b/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)



#CONE2

        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'sphere'
        m.child_frame_id = 'cone2'
        m.transform.translation.x = 0.0
        m.transform.translation.y = -0.5
        m.transform.translation.z = 0.0

        t1 = 1.0 * math.sin(math.radians(270/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(270/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 0.0 * math.sin(math.radians(b/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(b/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(c/2))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)


#CONE3

        t = tf.Transformer(True, rospy.Duration(10.0))
        m = geometry_msgs.msg.TransformStamped()
        now = rospy.get_rostime()
        m.header.stamp.secs = now.secs
        m.header.stamp.nsecs = now.nsecs
        m.header.frame_id = 'sphere'
        m.child_frame_id = 'cone3'
        m.transform.translation.x = 0.5
        m.transform.translation.y = 0.0
        m.transform.translation.z = 0.0

        t1 = 0.0 * math.sin(math.radians(a/2))
        t2 = 0.0 * math.sin(math.radians(a/2))
        t3 = 0.0 * math.sin(math.radians(a/2)) 
        t0 = 1.0 * math.cos(math.radians(a/2))

        r1 = 0.0 * math.sin(math.radians(b/2))
        r2 = 1.0 * math.sin(math.radians(270/2))
        r3 = 0.0 * math.sin(math.radians(b/2)) 
        r0 = 1.0 * math.cos(math.radians(270/2))

        w1 = r0*t1 + r1*t0 - r2*t3 - r3*t2
        w2 = r0*t2 + r1*t3 + r2*t0 - r3*t1
        w3 = r0*t3 - r1*t2 + r2*t1 + r3*t0
        w0 = r0*t0 - r1*t1 - r2*t2 - r3*t3

        q1 = 0.0 * math.sin(math.radians(c/2))
        q2 = 0.0 * math.sin(math.radians(c/2))
        q3 = 0.0 * math.sin(math.radians(c/2)) 
        q0 = 1.0 * math.cos(math.radians(0))

        m.transform.rotation.x = w0*q1 + w1*q0 - w2*q3 - w3*q2
        m.transform.rotation.y = w0*q2 + w1*q3 + w2*q0 - w3*q1
        m.transform.rotation.z = w0*q3 - w1*q2 + w2*q1 + w3*q0
        m.transform.rotation.w = w0*q0 - w1*q1 - w2*q2 - w3*q3

        aux = m.transform.translation.z
        t.setTransform(m)
        envio = []
        envio.append(m)
        pub.publish(envio)

        rate.sleep()        

if __name__ == '__main__':
    try:
        UAV3D()
    except rospy.ROSInterruptException:
        pass
