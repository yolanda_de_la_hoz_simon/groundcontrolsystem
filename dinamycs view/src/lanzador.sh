#!/bin/bash

if [ "$1" == "-s" ]
then
	source ../devel/setup.bash
	roslaunch uav3d sphere.launch model:=sphere.xml &
	sleep 2
	rosrun uav_controller uav_controller.py &
	sleep 2
	rosrun uav3d sphere.py 
	sleep 2
fi

if [ "$1" == "start" ] 
then
	source ../devel/setup.bash
	roslaunch uav3d display.launch model:=pelican.xml &
	sleep 2
	rosrun uav_controller uav_controller.py &
	sleep 2
	rosrun uav3d uav3d.py 
	sleep 2
fi

if [ "$1" == "stop" ] 
then
	killall python
	killall rviz
	PRUEBA=`ps -e | grep uav_controller`
	if [[ ! $PRUEBA == "" ]]
	then
		kill -9 ${PRUEBA:0:6}
	fi
fi

