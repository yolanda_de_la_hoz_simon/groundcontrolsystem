#!/usr/bin/python

import rospy
from Tkinter import *

from common.msg import Orders

class uavController(object):
	def __init__(self):
		print "Press on the Pad"
		self.publisher = rospy.Publisher("Orders", Orders, queue_size=10)
		self.controller()





	def controller(self):
		main = Tk()
		Label(text="Press Here").pack()
		Label(text="Controls").pack()
		Label(text="W").pack()
		Label(text="A S D").pack()
		Label(text="Space").pack()

		frame = Frame(main, width=100, height=0)
		frame.bind('<KeyPress>', self._onKeyPress)
		frame.bind('<space>', self._spaceKey)
		frame.bind('<Up>', self._upKey)
		frame.bind('<Right>', self._rightKey)
		frame.bind('<Left>', self._leftKey)
		frame.bind('<Down>', self._downKey)
		frame.focus_set()
		frame.pack()
		main.mainloop()



	def _onKeyPress(self,event):
		order= event.char
		self._sendOrder(order)

	def _upKey(self,event):
		order= "up"
		self._sendOrder(order)

	def _rightKey(self,event):
		order= "right"
		self._sendOrder(order)

	def _leftKey(self,event):
		order= "left"
		self._sendOrder(order)

	def _downKey(self,event):
		order= "down"
		self._sendOrder(order)

	def _spaceKey(self,event):
		order= "space"
		self._sendOrder(order)




	def _sendOrder(self,order = ""):
		message=Orders()
		message.type=order
		self.publisher.publish(message)


if __name__ == '__main__':
    rospy.init_node('uav_controller')
    try:
    	uavController()
    except rospy.ROSInterruptException: pass
